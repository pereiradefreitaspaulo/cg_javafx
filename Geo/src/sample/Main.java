package sample;

import javafx.application.Application;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import javafx.animation.ScaleTransition;

import javafx.animation.TranslateTransition;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Group root = new Group();
        Scene scene = new Scene(root, 1300, 700, Color.BLACK);
        scene.setFill(Color.WHITE);

        //Rectangle with LinearGradient background
        Stop[] stops = new Stop[] {
                new Stop(0, Color.BLACK),
                new Stop(1, Color.RED)};
        LinearGradient linearGradient =
                new LinearGradient(0, 0, 1, 0, true,
                        CycleMethod.NO_CYCLE, stops);

        Rectangle rect1 = new Rectangle(30, 60, 350, 300);
        rect1.setFill(linearGradient);
        rect1.setStroke(Color.RED);
        rect1.setStrokeWidth(10);

        RotateTransition rotateTransition1 =
                new RotateTransition(Duration.millis(2000), rect1);
        rotateTransition1.setByAngle(360);

        rect1.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            rotateTransition1.play();
        });

        Rectangle rect2 = new Rectangle(100, 200, 250, 250);
        rect2.setFill(Color.BLUEVIOLET);
        rect2.setStroke(Color.BLUE);
        rect2.setStrokeWidth(10);

        RotateTransition rotateTransition2 =
                new RotateTransition(Duration.millis(2000), rect2);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(2);
        rotateTransition2.setAutoReverse(true);

        rect2.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            rotateTransition2.play();
        });

        Rectangle rect3 = new Rectangle(1000, 100, 250, 200);
        rect3.setFill(Color.BLUEVIOLET);
        rect3.setStroke(Color.RED);
        rect3.setStrokeWidth(10);

        ScaleTransition scaleTransition1 = new ScaleTransition(Duration.millis(500), rect3);
        scaleTransition1.setByX(0.1f);
        scaleTransition1.setByY(0.1f);
        scaleTransition1.setCycleCount(4);
        scaleTransition1.setAutoReverse(true);

        rect3.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            scaleTransition1.play();
        });

        Rectangle rect4 = new Rectangle(1050, 150, 125, 100);
        rect4.setFill(Color.BLUEVIOLET);
        rect4.setStroke(Color.BLUE);
        rect4.setStrokeWidth(10);

        ScaleTransition scaleTransition2 = new ScaleTransition(Duration.millis(500), rect4);
        scaleTransition2.setByX(-0.1f);
        scaleTransition2.setByY(-0.1f);
        scaleTransition2.setCycleCount(4);
        scaleTransition2.setAutoReverse(true);

        rect4.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            scaleTransition2.play();
        });

        Rectangle rect5 = new Rectangle(500, 30, 350, 200);
        rect5.setFill(Color.BLUEVIOLET);
        rect5.setStroke(Color.RED);
        rect5.setStrokeWidth(10);

        TranslateTransition translateTransition1 = new TranslateTransition(Duration.millis(500), rect5);
        translateTransition1.setByX(40);
        translateTransition1.setByY(70);
        translateTransition1.setCycleCount(2);
        translateTransition1.setAutoReverse(true);
        rect5.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            translateTransition1.play();
        });

        Rectangle rect6 = new Rectangle(550, 100, 250, 200);
        rect6.setFill(Color.BLUEVIOLET);
        rect6.setStroke(Color.BLUE);
        rect6.setStrokeWidth(10);

        TranslateTransition translateTransition2 = new TranslateTransition(Duration.millis(500), rect6);
        translateTransition2.setByX(-40);
        translateTransition2.setByY(-70);
        translateTransition2.setCycleCount(2);
        translateTransition2.setAutoReverse(true);
        rect6.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            translateTransition2.play();
        });

        root.getChildren().addAll(rect1, rect2,rect3,rect4,rect5,rect6);

        primaryStage.setTitle("Trabalho CG!!!");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
